# Mini Project 10 
Rust Serverless Transformer Endpoint Project

# Rust Serverless Transformer Endpoint

This project deploys a Hugging Face transformer model, implemented in Rust, as a serverless function on AWS Lambda. The function is containerized using Docker and can be accessed via an HTTP endpoint.

## Requirements

- Docker
- AWS CLI configured with appropriate permissions
- Rust and Cargo

## Getting Started

### Clone the Repository

Start by cloning this repository to your local machine:

```bash
git clone https://github.com/your-repository/rust-transformer-lambda.git
cd rust-transformer-lambda
```

## Building the Docker Container

Navigate to the project directory and build the Docker container:
```
docker buildx build --progress=plain --platform linux/arm64 -t <your project name> .
```
## Deploying to AWS Lambda

Create an ECR repository:

```bash
aws ecr create-repository --repository-name <your repo name>
```

Tag your Docker image to match the repository, and push it to AWS ECR (follow the instruction on ECR)

Deploy the container to AWS Lambda:

```bash
aws lambda create-function --function-name <your function name> \
--package-type Image \
--code <image url> \
--role <your arn> \
--timeout 15 --memory-size 3008
```

## Setting Up the Endpoint

Create an API Gateway trigger to expose the Lambda function as an HTTP endpoint:

```bash
aws apigatewayv2 create-api --name rustTransformerApi --protocol-type HTTP --target <you arn>
```

## Usage

Once deployed, you can make requests to the endpoint using cURL:

```bash
curl -X POST "https://your-api-id.execute-api.us-east-1.amazonaws.com/" -d '{"input":"your query here"}'
```

## Screenshots

![Alt text](imgs/image.png)
![Alt text](imgs/image-1.png)
![Alt text](imgs/image-2.png)
![Alt text](imgs/image-3.png)